//
//  LocationsViewController.swift
//  uWeather
//
//  Created by student on 27.01.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import UIKit

class LocationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var zipcodes:[StoredValue] = [];
    var selectedValue:StoredValue = StoredValue(code:97074,city: "Würzburg");
    
    override func viewDidLoad() {
        super.viewDidLoad()
        zipcodes = StorageHelper().getCities();
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return zipcodes.count;
    }
    
//    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//        return true
//    }
//    
//    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        
//        switch editingStyle {
//        case .Delete:
//            let zip = self.zipcodes[indexPath.row].zip
//            let storageHelper = StorageHelper()
//            if storageHelper.deleteCity(zip){
//                print("Löschen")
//                zipcodes.removeAtIndex(indexPath.row)
//                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
//            }
//            
//        default:
//            print("mach nix")
//        }
//    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete") { (action, indexPath) in
            let zip = self.zipcodes[indexPath.row].zip
            let storageHelper = StorageHelper()
            if storageHelper.deleteCity(zip){
                Log.debug("Deleting row.")
                self.zipcodes.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            }

        }
        
        return [delete]
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            //Setup Table Cell
        if let cell = tableView.dequeueReusableCellWithIdentifier("cityCell", forIndexPath: indexPath) as? CityTableViewCell {
            cell.bind(zipcodes[indexPath.row]);
            return cell;
        }
            else {
                let cell = CityTableViewCell();
                cell.bind(zipcodes[indexPath.row]);
            return cell;
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedValue = zipcodes[indexPath.row];
        NetworkManager.getWeatherInfo(selectedValue.zip) { () -> Void in
            if(NetworkManager.weahter!.items.count == 0) {
                let alert = UIAlertController(title: "Sorry", message: "Cant get actual weather data :(", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }else {
                self.dismissViewControllerAnimated(true, completion: nil);
                StorageHelper().setCurrentCity(self.zipcodes[indexPath.row].name, zip: self.zipcodes[indexPath.row].zip);
            }
        }
    }
}
