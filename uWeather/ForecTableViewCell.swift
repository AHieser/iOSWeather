//
//  ForecTableViewCell.swift
//  uWeather
//
//  Created by student on 07.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import UIKit

class ForecTableViewCell: UITableViewCell {

    @IBOutlet weak var TempLabel: UILabel!
    @IBOutlet weak var StatusLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    
    var hours:[Item] = [Item]();
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func bind(items:[Item]) {
        if let value = items.first {
            TempLabel.text = String(format:"%.1f °C",value.main!.getTempMaxInCelsius());
            StatusLabel.text = String(format:"%.1f °C",value.main!.getTempMinInCelsius());
            let date = NSDate(timeIntervalSince1970: Double(value.dt));
            let formatter = NSDateFormatter()
            formatter.dateFormat = "EEEE";
            DateLabel.text = formatter.stringFromDate(date);
        }
    }
}