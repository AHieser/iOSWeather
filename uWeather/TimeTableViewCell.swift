//
//  TimeTableViewCell.swift
//  uWeather
//
//  Created by FHWS on 11.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import UIKit

class TimeTableViewCell: UITableViewCell {

   
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
    
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(value:Item) {
        let date = NSDate(timeIntervalSince1970: Double(value.dt));
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm:ss";
        timeLabel.text = formatter.stringFromDate(date);
        minLabel.text = String(format:"%.1f °C",value.main!.getTempMinInCelsius());
        maxLabel.text = String(format:"%.1f °C",value.main!.getTempMaxInCelsius());
    }

}
