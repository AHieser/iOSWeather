//
//  ForecastTableViewCell.swift
//  uWeather
//
//  Created by FHWS on 07.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var forecastMin: UILabel!
    @IBOutlet weak var forecastMax: UILabel!
    @IBOutlet weak var forecastDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(value:Item) {
        forecastDate.text = String(value.dt);
        forecastMax.text = "\(value.weather[0].main)"
        forecastMin.text = String(format: "%,1f",(value.main?.temp_min)!);
    }

}
