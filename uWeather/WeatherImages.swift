//
//  WeatherImages.swift
//  uWeather
//
//  Created by student on 10.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import Foundation
import UIKit

enum WeatherImages:String {
    case cloudy = "Cloudy"
    case foggy = "Foggy"
    case moony = "Moony"
    
    func getImage(key:String) -> UIImage {
        switch (key) {
            case "cloudy":
                return UIImage(named: .cloudy.toString)
        }
    }
}
