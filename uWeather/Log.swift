//
//  Log.swift
//  uWeather
//
//  Created by student on 11.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import Foundation

class Log {
    
    static func error(msg:String) {
        print("[ERROR]: \(msg)")
    }
    
    static func debug(msg:String) {
        print("[DEBUG]: \(msg)")
    }
}