//
//  WeatherInfo.swift
//  uWeather
//
//  Created by student on 30.01.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import Foundation

class WeatherInfo {
    
    var city:City?
    var items:[Item] = [Item]()
    
    init(){}
    
    init(city:City){
        self.city = city
    }
    
}

class Weather {
    var id:Int = 0;
    var main:String!
    var description:String!
    var icon:String?
    
    init(){}
    
    init(id:Int, main:String, description:String, icon:String){
        self.id = id
        self.main = main
        self.description = description
        self.icon = icon
    }
    
    func getImageString() -> String {
        
        switch self.main.lowercaseString {
            case "rain":
                return "Rainy"
            case "thunderstorm":
                return "Thunderstormy"
            case "clouds":
                return "Cloudy"
            case "atmosphere":
                return "Foggy"
            case "clear":
                return "Sunny"
            
        default:
            return "SunnyWithClouds"
            
        }
    }
    
}

class Item {
    // dt = day and time
    var dt:Int = 0
    
    var main:Main?
    var weather:[Weather] = [Weather]()
    var wind:Wind?
    var clouds:Cloud?
    var rain:Rain?
    
    init(){}
    
    func getDateTime() -> String {
        if self.dt != 0 {
            let timestamp = NSTimeInterval(Int(self.dt))
            let newtime = NSDate(timeIntervalSince1970:timestamp)
            return newtime.formattedStringForDate(DateFormat.dateTime)
        } else {
            return ""
        }
        
    }
}

class City {
    var id:Int = 0;
    var name:String?
    var country:String?
    
    init(){}
    
    init(id:Int, name:String, country:String){
        self.id = id
        self.name = name
        self.country = country
    }
    
}

class Main {
    var temp:Double = 0.0
    var pressure:Int = 0
    var humidity:Int = 0
    var temp_min:Double = 0.0
    var temp_max:Double = 0.0
    let kelvin_diff:Double = 273.75
    
    init(){}
    
    init(temp:Double, pressure: Int, humidity:Int, temp_min:Double, temp_max:Double,status:String){
        self.temp = temp
        self.pressure = pressure
        self.humidity = humidity
        self.temp_min = temp_min
        self.temp_max = temp_max
      
    }
    
    func calcToCelsius(ctemp:Double) -> Double {
        var celsius = 0.0
        if ctemp != 0.0 {
            celsius = ctemp-kelvin_diff
        }
        return celsius
    }
    
    func getTempInCelsius() -> Double {
        return self.calcToCelsius(temp)
    }
    
    func getTempMinInCelsius() -> Double {
        return self.calcToCelsius(temp_min)
    }
    
    func getTempMaxInCelsius() -> Double {
        return self.calcToCelsius(temp_max)
    }
}

class Cloud {
    var all:Int = 0
    
    init(){}
    
    init(all:Int){
        self.all = all
    }
}

class Wind {
    var speed:Double = 0.0
    
    init(){}
    
    init(speed:Double){
        self.speed = speed
    }
}

class Rain {
    var hit:Double = 0.0
    
    init(){}
    
    init(hit:Double){
        self.hit = hit
    }
}