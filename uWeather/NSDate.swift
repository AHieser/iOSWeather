//
//  NSDate.swift
//  uWeather
//
//  Created by student on 06.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import Foundation

extension NSDate {
    
    func formattedStringForDate(format: DateFormat) -> String {
        let formatter = NSDateFormatter()
        let locale = NSLocale.currentLocale(); //de_DE
        
        formatter.locale = locale
        formatter.dateFormat = format.rawValue
        
        return formatter.stringFromDate(self)
    }
}