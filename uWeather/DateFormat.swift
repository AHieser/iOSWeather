//
//  DateFormat.swift
//  uWeather
//
//  Created by student on 06.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import Foundation

enum DateFormat:String {
    case clockTime = "HH:mm:ss"
    case date = "dd-MM-YY"
    case dateTimeWithMilliseconds = "yyyy-MM-dd HH:mm:ss,SSS ZZZ"
    case dateTime = "yyyy-MM-dd HH:mm:ss"
}