//
//  AddLocationViewController.swift
//  uWeather
//
//  Created by student on 27.01.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import UIKit

class AddLocationViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var foudnCityLabel: UILabel!
    @IBOutlet weak var zipPicker: UIPickerView!
    @IBOutlet weak var saveButton: UIButton!
    
    
    private let pickerDataSource = Array(0...9)
    //init a high number for rows
    private let pickerRows = 10000
    //init zip
    var zipNumber:[String] = ["0", "0", "0", "0", "0"]
    var cityName:String? = ""
    var zipInt:Int = 0
    
    let UNKNOWN_CITY = "Unknown City"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.enabled = false
        
        self.zipPicker.delegate = self
        self.zipPicker.dataSource = self
        self.zipPicker.selectRow(pickerRows / 2, inComponent: 0, animated: true)
        self.zipPicker.selectRow(pickerRows / 2, inComponent: 1, animated: true)
        self.zipPicker.selectRow(pickerRows / 2, inComponent: 2, animated: true)
        self.zipPicker.selectRow(pickerRows / 2, inComponent: 3, animated: true)
        self.zipPicker.selectRow(pickerRows / 2, inComponent: 4, animated: true)
        
        //init label
        foudnCityLabel.text = self.UNKNOWN_CITY

    }
    

    func valueForRow(row: Int) -> Int {
        // the rows repeat every pickerViewData.count items
        return pickerDataSource[row % pickerDataSource.count]
    }
    
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        //return the value as String
        return "\(valueForRow(row))"
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        
        //zip code have 5 numbers
        return 5
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        //return the high number so it will look like the Picker is wrap around
        return pickerRows
    }
    
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        zipNumber[component] = "\(valueForRow(pickerView.selectedRowInComponent(component)))"
        
        self.zipInt = Int(zipNumber.joinWithSeparator(""))!
        
        NetworkManager.getWeatherInfo(self.zipInt, callback: {
            print(NetworkManager.customresponse.statuscode)
           
            print(NetworkManager.weahter?.city?.name)
            
            dispatch_async(dispatch_get_main_queue()) {
                self.cityName = NetworkManager.weahter?.city?.name
                
                if NetworkManager.customresponse.statuscode == 200 {
                    self.foudnCityLabel.text = self.cityName
                    self.saveButton.enabled = true
                } else {
                    self.foudnCityLabel.text = self.UNKNOWN_CITY
                    self.saveButton.enabled = false
                }
                    
            }
        });
    
    }
    
    
    @IBAction func saveButton(sender: UIButton) {
        let storageHelper:StorageHelper = StorageHelper()
        
        if(storageHelper.isThere(self.zipInt)) {
            Log.debug("Starting Alert, for already existing city.")
            let alert = UIAlertController(title: "Oh no!", message: "City is already in List :(", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            storageHelper.saveCity(self.cityName!, zip: self.zipInt)
            storageHelper.setCurrentCity(self.cityName!, zip: self.zipInt)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    

}
