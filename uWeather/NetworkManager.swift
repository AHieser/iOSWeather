//
//  NetworkManager.swift
//  uWeather
//
//  Created by student on 30.01.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import Foundation

class NetworkManager {
    
    // API for openweathermap.org - Requests
    static var api = "ce9df334a30bc6c1905b87c24abef86e"
    static var weahter:WeatherInfo?
    static var customresponse:Response = Response()
    
    
    //
    static var uri = "http://api.openweathermap.org/data/2.5/forecast"
    //static var uri = "http://api.openweathermap.org/data/2.5/weather"
    
    // Starts the request to the api with a forecast of 5 days
    static func getWeatherInfo(city: Int, callback: () -> Void){
        // Init empty WeatherInfo
        let weatherinfo:WeatherInfo = WeatherInfo()
        
        // Dynamic Uri
        let weatherSwiftUrl = NSURL(string: "\(uri)?zip=\(city),de&APPID=\(api)")
        
        //NSURL Stuff
        let request = NSURLRequest(URL: weatherSwiftUrl!)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        session.dataTaskWithRequest(request, completionHandler: {(data, response, error)in

            guard data != nil else {
                return
            }
            
            var jsonResult: NSDictionary!
            do{
                jsonResult = try (NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary)!
                
                customresponse.statuscode = (response as! NSHTTPURLResponse).statusCode
                
                guard let jsonResultUnwrapped = jsonResult else {
                    return
                }
                
                // StatusCode festlegen
                if let jsonStatusCode = jsonResultUnwrapped["cod"] {
                    NetworkManager.customresponse.statuscode = Int(jsonStatusCode as! String)!
                }
                
                // Json will be mapped to the Swift Object WeatherInfo
                
                // City
                if let jsonCity = jsonResultUnwrapped["city"] {
                    weatherinfo.city = City()
                    weatherinfo.city?.name = (jsonCity["name"] as? String)
                    weatherinfo.city?.id = (jsonCity["id"] as? Int) ?? 0
                    weatherinfo.city?.country = (jsonCity["country"] as? String)
                }
                
                // Items
                
                if let listJson:NSArray = jsonResultUnwrapped["list"] as? NSArray {
                    
                    for itemJson in listJson {
                        
                        let item:Item = Item()
                        
                        // Main
                        if let jsonMain = itemJson["main"] {
                            item.main = Main()
                            item.main!.temp = (jsonMain!["temp"] as? Double) ?? 0.0
                            item.main!.humidity = (jsonMain!["humidity"] as? Int) ?? 0
                            item.main!.pressure = (jsonMain!["pressure"] as? Int) ?? 0
                            item.main!.temp_max = (jsonMain!["temp_max"] as? Double) ?? 0.0
                            item.main!.temp_min = (jsonMain!["temp_min"] as? Double) ?? 0.0
                        
                            
                        }
                        
                        // Weather
                        if let jsonWeathers:NSArray = itemJson["weather"]! as? NSArray {
                            
                            for weatherJson in jsonWeathers {
                                let weather:Weather = Weather()
                                weather.id = (weatherJson["id"] as? Int)!
                                weather.main = (weatherJson["main"] as? String)!
                                weather.description = (weatherJson["description"] as? String)
                                weather.icon = (weatherJson["icon"] as? String)
                                item.weather.append(weather)
                            }
                        }
                        
                        // Wind
                        if let jsonWind = itemJson["wind"] {
                            item.wind = Wind()
                            item.wind!.speed = jsonWind!["speed"] as? Double ?? 0.0
                        }
                        
                        // Cloud
                        if let cloudJson = itemJson["clouds"] {
                            item.clouds = Cloud(all: cloudJson!["all"] as? Int ?? 0)
                        }
                        
                        // Rain
                        if let rainJson = itemJson["rain"] {
                            if let rainAttrJson = rainJson {
                                item.rain = Rain(hit: rainAttrJson as? Double ?? 0.0)
                            }
                        }
                        
                        // Dates
                        if let time = itemJson["dt"] as? Int {
                            item.dt = time;
                        }
                        
                        // Adding item to the list in weatherinfo
                        weatherinfo.items.append(item)
                    }
                    
                    
                    self.weahter = weatherinfo;
                    
                } else {
                    NetworkManager.customresponse.statuscode = 400
                    self.weahter?.items.removeAll();
                }
                callback();

            }catch{
                Log.error("Couldn't parse the response.")
                return
            }
            
            return
        }).resume()
    }
}