//
//  CityTableViewCell.swift
//  uWeather
//
//  Created by FHWS on 06.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {

    @IBOutlet weak var CityName: UILabel!
    @IBOutlet weak var CityZip: UILabel!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(value:StoredValue) {
        CityName.text = value.name;
        CityZip.text = (String(value.zip));
    }

}
