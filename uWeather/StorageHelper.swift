//
//  StorageHelper.swift
//  uWeather
//
//  Created by FHWS on 06.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import Foundation

class StorageHelper {
    
    init() {
        
    }
    
    // Saves a city with name and zip to the UserDefaults
    func saveCity(name:String, zip: Int) {
        var cities = self.getCities();
        cities.append(StoredValue(code: zip, city: name));
        let data = NSKeyedArchiver.archivedDataWithRootObject(cities);
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "zipcodes");
        Log.debug("Zip (\(name), \(zip)) was successfully saved in zipcodes.")
    }
    
    // Deletes a city by the given zip id
    func deleteCity(zip: Int) -> Bool {
        var deleted:Bool = false
        var cities:[StoredValue] = self.getCities()
        for (index, city) in cities.enumerate() {
            if city.zip == zip {
                cities.removeAtIndex(index)
                deleted = true
                break
            }
        }
        if deleted {
            let data = NSKeyedArchiver.archivedDataWithRootObject(cities);
            NSUserDefaults.standardUserDefaults().setObject(data, forKey: "zipcodes");
            Log.debug("Deleted \(zip).")
        } else {
            Log.error("Couldn't delete \(zip).")
        }
        return deleted
    }
    
    // Returns alls cities which are stored in the UserDefauls
    func getCities() -> [StoredValue] {
        //Forced downcast because we know exactly whats in the array, so we dont worry about cast exception
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("zipcodes") as? NSData {
            var cities = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! [StoredValue];
            for city in cities {
                print(city);
            }
            cities = cities.sort { $0.name < $1.name }
            return  cities;
        } else {
            Log.error("Couldn't find any stored cities.")
        }
        return  [StoredValue]();
    }
    
    // Saves the current selected city
    func setCurrentCity(name:String, zip:Int){
        let storedValue = StoredValue(code: zip, city: name);
        let data = NSKeyedArchiver.archivedDataWithRootObject(storedValue);
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "curcity");
        Log.debug("Zip (\(name), \(zip)) was successfully saved in curcity.")
    }
    
    // Returns the current selected city
    func getCurrentCity() -> StoredValue {
        var storedValue = StoredValue()
        if let data = NSUserDefaults.standardUserDefaults().objectForKey("curcity") as? NSData {
            storedValue = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! StoredValue;
            Log.debug("Loaded current city \(storedValue.zip)")
        } else {
            Log.error("Couldn't find any stored citiy in curcity.")
        }
        return storedValue
    }
    
    // Checks if the requested city is already saved in the UserDefaults
    func isThere(zip: Int) -> Bool {
        //Check if City is already in List
        let allCities:[StoredValue] = self.getCities()
        //run through all cities
        for city in allCities {
            if(city.zip == zip) {
                Log.debug("Zip exists.")
                return true
            }
        }
        return false
    }
    
}

class StoredValue : NSObject {
    
    var name:String!
    var zip:Int!
    
    override init(){}
    
    init(code:Int,city:String) {
        self.name = city;
        self.zip = code;
        super.init()        // call NSObject's init method
    }
    
    
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(name, forKey: "name");
        aCoder.encodeObject(zip,forKey:"zip");
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard
            let name = aDecoder.decodeObjectForKey("name") as? String,
            let zip = aDecoder.decodeObjectForKey("zip") as? Int
            
            else{
                return nil
        };
        self.init(code:zip,city:name);
    }
}

