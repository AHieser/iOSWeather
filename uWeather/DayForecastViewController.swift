//
//  DayForecastViewController.swift
//  uWeather
//
//  Created by FHWS on 11.02.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import UIKit

class DayForecastViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var items:[Item] = [Item]();
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("TimeCell", forIndexPath: indexPath) as? TimeTableViewCell {
            cell.bind(self.items[indexPath.row]);
            return cell;
        }
        else {
            let cell = TimeTableViewCell();
            cell.bind(self.items[indexPath.row]);
            return cell;
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.dismissViewControllerAnimated(true, completion: nil);

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }

}
