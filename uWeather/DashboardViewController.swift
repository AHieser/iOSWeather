//
//  DashboardViewController.swift
//  uWeather
//
//  Created by student on 27.01.16.
//  Copyright © 2016 alexiphilidomi. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    @IBOutlet weak var forecastTable: UITableView!
    
    var forecastList:[String: [Item]] = [String:[Item]]();
    var keyList:[String] = [String]();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkForFirstStart()
        
      
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
    let def = StorageHelper().getCurrentCity();
            NetworkManager.getWeatherInfo(def.zip,callback: {
                print(NetworkManager.weahter?.city?.name);
                print(NetworkManager.customresponse.statuscode)
                  self.updateUI(NetworkManager.weahter!)
                }
            );
    }
    
    
    func updateUI(weather:WeatherInfo) {
        dispatch_async(dispatch_get_main_queue()){
            if weather.items.count != 0  {
                self.forecastList = self.prepareForecast(weather.items)
                self.cityLabel.text = weather.city?.name;
                self.tempLabel.text = String(format:"%.1f",weather.items[0].main!.getTempInCelsius());
                self.statusLabel.text = String(weather.items[0].weather[0].description)
                self.forecastTable.reloadData();
                self.forecastTable.reloadInputViews()
                let image: UIImage = UIImage(named: weather.items[0].weather[0].getImageString())!
                self.weatherImage.image = image
              
            }else {
                let alert = UIAlertController(title: "Sorry", message: "Cant get actual weather data :(", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil);
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            if let cell = tableView.dequeueReusableCellWithIdentifier("ForecastCell", forIndexPath: indexPath) as? ForecTableViewCell {
                    cell.bind(self.forecastList[self.keyList[indexPath.row]]!);
                    return cell;
            }
            else {
                let cell = ForecTableViewCell();
                  cell.bind(self.forecastList[self.keyList[indexPath.row]]!);
                    return cell;
            }
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecastList.count;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let storyboard : UIStoryboard = UIStoryboard(
            name: "Main",
            bundle: nil)
        let menuViewController: DayForecastViewController = storyboard.instantiateViewControllerWithIdentifier("DayCastPopover") as! DayForecastViewController
        
        menuViewController.items = self.forecastList[self.keyList[indexPath.row]]!;
        
        menuViewController.modalPresentationStyle = .Popover
        menuViewController.preferredContentSize = CGSizeMake(50, 50)
        
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = .Any
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = tableView
        popoverMenuViewController?.sourceRect = CGRect(
            x: 50,
            y: 50,
            width: 50,
            height: 50)
        presentViewController(
            menuViewController,
            animated: true,
            completion: nil)
        
    }
    
    func loadCityWeather(zip:Int) {
        NetworkManager.getWeatherInfo(zip,callback:  {
            self.updateUI(NetworkManager.weahter!);
        });
    }
    
    func checkForFirstStart() {
        let defaults = NSUserDefaults.standardUserDefaults();
        let firstStart = defaults.boolForKey("firstStart") ?? false;
        if(!firstStart) {
            var defaultZipCodes:[StoredValue] = [StoredValue]();
            let value = StoredValue(code:97074, city: "Würzburg");
            defaultZipCodes.append(value);
            let data = NSKeyedArchiver.archivedDataWithRootObject(defaultZipCodes);
            defaults.setObject(data, forKey: "zipcodes");
            defaults.setBool(true, forKey: "firstStart");
            StorageHelper().setCurrentCity("Würzburg", zip: 97074)
            defaults.synchronize();
        }
    }
    
    func prepareForecast(items:[Item]) -> [String:[Item]] {
        var dateCastList = [String: [Item]]();
        let formatter = NSDateFormatter()
        formatter.dateFormat = "EEEE";
        for item in items
        {
            let date = NSDate(timeIntervalSince1970: Double(item.dt));
            let day = formatter.stringFromDate(date);
            if dateCastList.keys.contains(day) {
                dateCastList[day]?.append(item)
            }else {
                dateCastList[day] = [item];
                self.keyList.append(day);
            }
        }
        return dateCastList;
    }

}
